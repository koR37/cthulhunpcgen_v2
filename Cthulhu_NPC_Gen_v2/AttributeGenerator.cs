﻿using System;

namespace Cthulhu_NPC_Gen_v2
{
    public class AttributeGenerator
    {
        public static void CubeRandomAttributes(Npc npc)
        {
            /*  
             * Method 1: (3D6) * 5
             * Method 2: (2D6 + 6) * 5
             */
            
            // Method 1
            npc.Strength = Dice.Cube(6, 3, 0, 5);
            npc.Constitution = Dice.Cube(6, 3, 0, 5);
            npc.Dexterity = Dice.Cube(6, 3, 0, 5);
            npc.Appearance = Dice.Cube(6, 3, 0, 5);
            npc.Mana = Dice.Cube(6, 3, 0, 5);
            // Method 2
            npc.Intelligence = Dice.Cube(6, 2, 6, 5);
            npc.Education = Dice.Cube(6, 2, 6, 5);
            npc.Size = Dice.Cube(6, 2, 6, 5);

            CalculateSecondary(npc);
        }

        public static void CubeWeightedAttributes(Npc npc)
        {
            int[,] weights = Constants.GetWeights(npc.Archetype);

            // Size, Intelligence and Education get calculated by another method so choose value 1 of weights.
            npc.Strength = Dice.WeightedCube(weights[0, (int)Attributes.Strength], weights[1, (int)Attributes.Strength], 0);
            npc.Constitution = Dice.WeightedCube(weights[0, (int)Attributes.Constitution], weights[1, (int)Attributes.Constitution], 0);
            npc.Size = Dice.WeightedCube(weights[0, (int)Attributes.Size], weights[1, (int)Attributes.Size], 1);
            npc.Dexterity = Dice.WeightedCube(weights[0, (int)Attributes.Dexterity], weights[1, (int)Attributes.Dexterity], 0);
            npc.Appearance = Dice.WeightedCube(weights[0, (int)Attributes.Appearance], weights[1, (int)Attributes.Appearance], 0);
            npc.Intelligence = Dice.WeightedCube(weights[0, (int)Attributes.Intelligence], weights[1, (int)Attributes.Intelligence], 1);
            npc.Mana = Dice.WeightedCube(weights[0, (int)Attributes.Mana], weights[1, (int)Attributes.Mana], 0);
            npc.Education = Dice.WeightedCube(weights[0, (int)Attributes.Education], weights[1, (int)Attributes.Education], 1);

            CalculateSecondary(npc);
        }

        public static void CalculateSecondary(Npc npc)
        {
            Random rnd = Dice.Rnd;

            npc.Hitpoints = (npc.Constitution + npc.Size) / 10;
            npc.Magicpoints = npc.Mana / 5;
            npc.Luck = Dice.Cube(6, 3, 0, 5);
            npc.Stability = npc.Mana;

            //Calculate DamageBonus and Stature
            int temp = npc.Strength + npc.Size;
            if (temp >= 2 && temp <= 64) { npc.DamageBonus = -2; npc.Stature = -2; }
            else if (temp >= 65 && temp <= 84) { npc.DamageBonus = -1; npc.Stature = -1; }
            else if (temp >= 85 && temp <= 124) { npc.DamageBonus = 0; npc.Stature = 0; }
            else if (temp >= 125 && temp <= 204) { npc.DamageBonus = Dice.Cube(4, 1, 0, 1); npc.Stature = 1; }
            else if (temp >= 165 && temp <= 284) { npc.DamageBonus = Dice.Cube(6, 1, 0, 1); npc.Stature = 2; }

            // Calculate Movement
            if (npc.Dexterity < npc.Size && npc.Strength < npc.Size) { npc.Movement = 7; }
            else if (npc.Dexterity > npc.Size || npc.Strength > npc.Size || npc.Strength + npc.Dexterity == npc.Size * 2) { npc.Movement = 8; }
            else if (npc.Dexterity > npc.Size && npc.Strength > npc.Size) { npc.Movement = 9; }
            else { npc.Movement = 6; }

            // Age modifier for Movement
            if (npc.Age >= 40) { --npc.Movement; }
            else if (npc.Age >= 50) { npc.Movement -= 2; }
            else if (npc.Age >= 60) { npc.Movement -= 3; }
            else if (npc.Age >= 70) { npc.Movement -= 4; }
            else if (npc.Age >= 80) { npc.Movement -= 5; }
        }
    }
}
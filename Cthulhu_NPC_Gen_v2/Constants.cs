﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Cthulhu_NPC_Gen_v2
{
    public enum Ethnicity
    {
        [Description("firstname + lastname")]
        English,
        [Description("firstname + lastname")]
        AfricanAmerican,
        [Description("firstname + lastname")]
        German,
        [Description("firstname + lastname")]
        French,
        [Description("firstname + lastname")]
        Arabian,
        [Description("firstname + malename")]
        Algerian,
        [Description("firstname + lastname")]
        Egyptian,
        [Description("firstname + malename + malename")]
        Ethiopian,
        [Description("firstname")]
        Akkadian,
        [Description("firstname")]
        NativeAmerican,
        [Description("firstname")]
        Assyrian,
        [Description("firstname")]
        Sumerian,
        [Description("lastname")]
        Inuit,
        [Description("firstname + lastname + malename")]
        Tuareg
    }

    public enum Gender
    {
        [Description("Männlich")]
        Male,
        [Description("Weiblich")]
        Female,
        [Description("Sonstige")]
        Other
    }

    public enum Attributes
    {
        Strength,
        Constitution,
        Size,
        Dexterity,
        Appearance,
        Intelligence,
        Mana,
        Education
    }

    public enum Archetype
    {
        Commoner,
        Athletic,
        Brute,
        Academic,
        Social,
        Retard
    }

    public class Constants
    {
        // First dimension indicates min or max, second dimension the actual value.

        // Attributes                                               ST, KO, GR, GE, ER, IN, MA, BI
        private static int[,] CommonerWeights = new int[2, 8]    { {15, 15, 15, 15, 15, 40, 15, 40},
                                                                   {80, 80, 80, 80, 80, 90, 80, 90} };

        private static int[,] AthleticWeights = new int[2, 8]    { {15, 50, 15, 50, 50, 40, 15, 40},
                                                                   {80, 80, 80, 80, 80, 60, 60, 60} };

        private static int[,] BruteWeights = new int[2, 8]       { {50, 50, 50, 15, 15, 40, 15, 40},
                                                                   {80, 80, 80, 80, 80, 60, 60, 60} };

        private static int[,] AcademicWeights = new int[2, 8]    { {15, 15, 15, 15, 15, 50, 50, 50},
                                                                   {60, 60, 80, 60, 80, 90, 80, 90} };

        private static int[,] SocialWeights = new int[2, 8]      { {15, 15, 15, 15, 50, 50, 15, 50},
                                                                   {60, 60, 80, 80, 80, 90, 60, 90} };

        private static int[,] RetardWeights = new int[2, 8]      { {15, 15, 15, 15, 15, 10, 10, 10},
                                                                   {80, 80, 80, 50, 80, 35, 35, 35} };

        public static int[,] GetWeights(Archetype Archetype)
        {
            int[,] weights = new int[2, 8];

            switch (Archetype)
            {
                case Archetype.Commoner:
                    weights = CommonerWeights;
                    break;
                case Archetype.Athletic:
                    weights = AthleticWeights;
                    break;
                case Archetype.Brute:
                    weights = BruteWeights;
                    break;
                case Archetype.Academic:
                    weights = AcademicWeights;
                    break;
                case Archetype.Social:
                    weights = SocialWeights;
                    break;
                case Archetype.Retard:
                    weights = RetardWeights;
                    break;
            }

            return weights;
        }

        public static string ToDescription(Enum en) //ext method
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }
            return en.ToString();
        }
    }
}
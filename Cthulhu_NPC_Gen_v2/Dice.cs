﻿using System;

namespace Cthulhu_NPC_Gen_v2
{
    public class Dice
    {
        public static Random Rnd = new System.Random();

        /// <summary>
        /// Cubes a Dice with a specific eyecount, options for multiplier, secondarymultiplier and an addition.
        /// </summary>
        /// <param name="amountDice"> Specifies the amount of dice rolls. </param>
        /// <param name="addition"> Added after the first multiplication. </param>
        /// <param name="secondarymultiplier"> Multiplied with this number at last. </param>
        public static int Cube(int eyecount, int amountDice, int addition, int secondarymultiplier)
        {
            int value = 0;

            // Choose Multipliers of 1 and addition of 0 to get a normal roll

            for (int i = 1; i <= amountDice; i++)
            {
                value += Rnd.Next(eyecount + 1);
            }

            return (value + addition) * secondarymultiplier;
        }

        /// <summary>
        /// Cubes a Dice with weighted results depending on the chosen method.
        /// </summary>
        /// <param name="min"> Specifies the minimum range of the value. </param>
        /// <param name="max"> Specifies the maximum range of the value. </param>
        /// <param name="method">
        /// 0 = (3D6 + 0) * 5;
        /// 1 = (2D6 + 6) * 5. 
        /// </param>
        /// <returns> A value between the chosen min and max parameter. </returns>
        public static int WeightedCube(int min, int max, int method)
        {
            int value = -1;

            //Console.WriteLine("# Min: " + min + " /// # Max: " + max + " /// # Method: " + method);

            while (!(value >= min && value <= max))
            {
                switch (method)
                {
                    case 0:
                        value = Cube(6, 3, 0, 5);
                        break;
                    case 1:
                        value = Cube(6, 2, 6, 5);
                        break;
                    default:
                        value = Cube(6, 3, 0, 5);
                        break;
                }
            }

            //Console.WriteLine("## Value: " + value);

            return value;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cthulhu_NPC_Gen_v2
{
    public class Job
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("EducationModifier")]
        public int EducationModifier { get; set; }

        [JsonProperty("SecondAttributes")]
        public string[] SecondAttributes { get; set; }
        [JsonProperty("SecondAttributeModifer")]
        public int SecondAttributeModifer { get; set; }

        [JsonProperty("Finances")]
        public int[] Finances { get; set; }
        [JsonProperty("Contacts")]
        public string Contacts { get; set; }

        [JsonProperty("Skills")]
        public string[] Skills { get; set; }
        [JsonProperty("SoftSkillAmount")]
        public int SoftSkillAmount { get; set; }
        [JsonProperty("SkillChoiceAmount")]
        public int SkillChoiceAmount { get; set; }
        [JsonProperty("SkillChoices")]
        public string[] SkillChoices { get; set; }
        
        public override string ToString()
        {
            string output = "=== Start Job ===\n" +
                            Name + "\n" +
                            "EduMod: " + EducationModifier + "\n" +
                            "SecAtts: " + string.Join(",", SecondAttributes) + "\n" +
                            "SecAttMod: " + SecondAttributeModifer + "\n" +
                            "Finances: " + string.Join(",", Finances) + "\n" +
                            "Contacts: " + Contacts + "\n" +
                            "Skills: " + string.Join(", ", Skills) + "\n" +
                            "SoftSkills: " + SoftSkillAmount + "\n" +
                            "SkillAmount: " + SkillChoiceAmount + "\n" +
                            "SkillChoices: " + string.Join(",", SkillChoices) + "\n" +
                            "=== End Job ===\n";

            return output;
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cthulhu_NPC_Gen_v2
{
    public class JsonHandler
    {
        private const string JobPath = @"..\..\res\data\Jobs.json";
        private const string SkillsPath = @"..\..\res\data\Skills.json";

        public static Job[] JobList = DeserializeJobList();
        public static Skill[] SkillList = DeserializeSkillList();

        public static Job[] DeserializeJobList()
        {
            string jsonString = File.ReadAllText(JobPath);
            Job[] jobList = JsonConvert.DeserializeObject<Job[]>(jsonString);
            Console.WriteLine("->\tJobList successfully deserialized\t+++");
            return jobList;
        }

        public static Skill[] DeserializeSkillList()
        {
            string jsonString = File.ReadAllText(SkillsPath);
            Skill[] skillList = JsonConvert.DeserializeObject<Skill[]>(jsonString);
            Console.WriteLine("->\tSkillList successfully deserialized\t+++\n");
            return skillList;
        }
    }
}
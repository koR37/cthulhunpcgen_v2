﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Cthulhu_NPC_Gen_v2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public struct ListViewItem
        {
            public string SkillName { get; set; }
            public int Value { get; set; }
            public int HalfValue { get; set; }
            public int QuarterValue { get; set; }

            public ListViewItem(string skillName, int value)
            {
                SkillName = skillName;
                Value = value;
                HalfValue = value / 2;
                QuarterValue = value / 4;
            }
        }

        public ObservableCollection<ComboBoxItem> CbGenderItems { get; set; }
        public ComboBoxItem SelectedGenderCbItem { get; set; }

        public ObservableCollection<ComboBoxItem> CbArchetypeItems { get; set; }
        public ComboBoxItem SelectedArchetypeCbItem { get; set; }

        public ObservableCollection<ComboBoxItem> CbEthnicityItems { get; set; }
        public ComboBoxItem SelectedEthnicityCbItem { get; set; }
        
        private Npc Npc;
        public static Npc LastNpc;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            CbGenderItems = new ObservableCollection<ComboBoxItem>();
            CbArchetypeItems = new ObservableCollection<ComboBoxItem>();
            CbEthnicityItems = new ObservableCollection<ComboBoxItem>();

            ComboBoxItem cbGenderItem = new ComboBoxItem { Content = "Auswählen" };
            ComboBoxItem cbArchetypeItem = new ComboBoxItem { Content = "Auswählen" };
            ComboBoxItem cbEthnicityItem = new ComboBoxItem { Content = "Auswählen" };

            SelectedGenderCbItem = cbGenderItem;
            SelectedArchetypeCbItem = cbArchetypeItem;
            SelectedEthnicityCbItem = cbEthnicityItem;

            CbGenderItems.Add(cbGenderItem);
            CbArchetypeItems.Add(cbArchetypeItem);
            CbEthnicityItems.Add(cbEthnicityItem);

            foreach (Gender gender in Enum.GetValues(typeof(Gender)))
            {
                CbGenderItems.Add(new ComboBoxItem { Content = Constants.ToDescription(gender) });
            }
            foreach (string archetype in Enum.GetNames(typeof(Archetype)))
            {
                CbArchetypeItems.Add(new ComboBoxItem { Content = archetype });
            }
            foreach (string ethnicity in Enum.GetNames(typeof(Ethnicity)))
            {
                CbEthnicityItems.Add(new ComboBoxItem { Content = ethnicity });
            }

            LockButton();
        }

        private void CloseApp_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MinimizeApp_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        public void DragWindow(object sender, MouseButtonEventArgs args)
        {
            DragMove();
        }
        
        private void BtnGenerate_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("\n\n\n");
            LockButton();
            BuildNpc();
            PrintNpc();
        }

        public void LockButton()
        {
            bool isAgeConvertible = false;
            isAgeConvertible = int.TryParse(txtAge.Text, out int age);

            if (cboGender.Text == "Auswählen" || cboArchetype.Text == "Auswählen" || cboEthnicity.Text == "Auswählen" || !isAgeConvertible)
            {
                btnGenerate.IsEnabled = false;
            }
            else
            {
                btnGenerate.IsEnabled = true;
            }
        }

        public void BuildNpc()
        {
            SkillListView.Items.Clear();

            Gender gender = Gender.Male;
            Archetype archetype = Archetype.Commoner;
            Ethnicity ethnicity = Ethnicity.Sumerian;

            // Check combobox items.
            foreach (Gender legitGender in Enum.GetValues(typeof(Gender)))
            {
                string enumDescription = Constants.ToDescription(legitGender);

                ComboBoxItem genderItem = (ComboBoxItem)cboGender.SelectedItem;
                string genderToCheck = genderItem.Content.ToString();

                if (enumDescription == genderToCheck)
                {
                    gender = legitGender;
                    Console.WriteLine("---> Legit Gender found: {0}", gender);
                }
            }

            foreach (Archetype legitArchetype in Enum.GetValues(typeof(Archetype)))
            {
                ComboBoxItem archeTypeItem = (ComboBoxItem)cboArchetype.SelectedItem;
                string ArchetypeToCheck = archeTypeItem.Content.ToString();

                if (legitArchetype.ToString() == ArchetypeToCheck)
                {
                    archetype = legitArchetype;
                    Console.WriteLine("---> Legit Archetype found: {0}", archetype);
                }
            }

            foreach (Ethnicity legitEthnicity in Enum.GetValues(typeof(Ethnicity)))
            {
                ComboBoxItem ethnicityItem = (ComboBoxItem)cboEthnicity.SelectedItem;
                string ethnicityToCheck = ethnicityItem.Content.ToString();

                if (legitEthnicity.ToString() == ethnicityToCheck)
                {
                    ethnicity = legitEthnicity;
                    Console.WriteLine("---> Legit Ethnicity found: {0}\n", ethnicity);
                }
            }

            // Generate npc from information given in GUI
            Npc = NpcGenerator.GenerateNpc(int.Parse(txtAge.Text), gender, archetype, ethnicity);
            LastNpc = Npc;

            // Sort Npc.SkillSet and add to ListView.
            Npc.SkillSet.Sort(new SortSkill());
            foreach (Skill skill in Npc.SkillSet)
            {
                SkillListView.Items.Add(SkillToListViewItem(skill));
            }
        }

        public ListViewItem SkillToListViewItem(Skill skill)
        {
            return new ListViewItem(skill.Name, skill.Value);
        }

        // Print npc to GUI
        public void PrintNpc()
        {
            txtName.Text = Npc.Firstname + " " + Npc.Lastname;
            txtAge.Text = Npc.Age.ToString();
            txtJob.Text = Npc.Job;

            txtStrength.Text = Npc.Strength.ToString();
            txtConstitution.Text = Npc.Constitution.ToString();
            txtSize.Text = Npc.Size.ToString();
            txtDexterity.Text = Npc.Dexterity.ToString();
            txtAppearance.Text = Npc.Appearance.ToString();
            txtIntelligence.Text = Npc.Intelligence.ToString();
            txtMana.Text = Npc.Mana.ToString();
            txtEducation.Text = Npc.Education.ToString();

            txtMovement.Text = Npc.Movement.ToString();
            txtLuck.Text = Npc.Luck.ToString();
            txtHitpoints.Text = Npc.Hitpoints.ToString();
            txtMagicPoints.Text = Npc.Magicpoints.ToString();
            txtStature.Text = Npc.Stature.ToString();
            txtDamageBonus.Text = Npc.DamageBonus.ToString();
            txtStability.Text = Npc.Stability.ToString();

            txtTrelloOutput.Text = TrelloOutput.PrintTrelloOutput(Npc);
        }

        private void Cbo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LockButton();
        }

        private void Cbo_DropDownClosed(object sender, EventArgs e)
        {
            LockButton();
        }

        public void Txt_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= Txt_GotFocus;
        }

        public void Txt_LostFocus(object sender, RoutedEventArgs e)
        {
            LockButton();
        }

        // Test button function for debugging.
        SkillSetHandler skillSetHandler = new SkillSetHandler();
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //string exampleString = "Naturwissenschaft(Biologie|Pharmazie)";
            //Console.WriteLine(skillSetHandler.GetChoice(exampleString));


            //string exampleString = "Fernkampf(Gewehr/Flinte|Shotgun|Flammenwerfer)";
            //Console.WriteLine(skillSetHandler.GetParenthesisContent(exampleString));


            //string exampleString = "Fernkampf(Gewehr/Flinte|Schrotflinte|Flammenwerfer)#Naturwissenschaft(Biologie|Pharmazie)";
            //Console.WriteLine(skillSetHandler.GetSkillFromString(exampleString));


            //int exampleAmount = 5;
            //Skill[] skills = skillSetHandler.GetRandomSkills(exampleAmount);
            //foreach (Skill skill in skills)
            //{
            //    Console.WriteLine("Skillname: " + skill.Name);
            //}
            //Console.WriteLine("---");


            //string exampleString = "Nahkampf";
            //Skill[] skills = skillSetHandler.GetSpecialisationsOfSkill(exampleString);
            //foreach (Skill skill in skills)
            //{
            //    Console.WriteLine("Skillname: " + skill.Name);
            //}
            //Console.WriteLine("---");


            //Job[] jobs = skillSetHandler.JobList;
            //Job job = jobs[2];
            //Skill[] skills = skillSetHandler.GetRandomSkillChoices(job);
            //Console.WriteLine("JobName: " + job.Name + "\n---\n");
            //foreach (Skill skill in skills)
            //{
            //    Console.WriteLine("Skillname: " + skill.Name);
            //}
            //Console.WriteLine("---");


            //Console.WriteLine(skillSetHandler.GetRandomSpecialisation("Naturwissenschaft"));


            //Console.WriteLine(skillSetHandler.GetRandomSkillOfType("academic"));


            //string exampleString = "academic";
            //Skill[] skills = skillSetHandler.GetRandomSkillOfTypeArray(exampleString, 5);
            //foreach (Skill skill in skills)
            //{
            //    Console.WriteLine("Skillname: " + skill.Name);
            //}
            //Console.WriteLine("---");
        }
    }
}
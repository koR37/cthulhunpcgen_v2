﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cthulhu_NPC_Gen_v2
{
    public class NameGenerator
    {
        public static string[] GenerateName(Npc npc)
        {
            string[] name = {"", ""};
            List<string> firstName = npc.Gender == Gender.Male ? NamelistHandler.MaleNames : NamelistHandler.FemaleNames;
            
            string nameScheme = Constants.ToDescription(npc.Ethnicity);
            Console.WriteLine("---> Name generated for: \"" + npc.Gender + "\" with namescheme: \"" + nameScheme + "\"");

            int firstNamesLength = firstName.Count();
            int lastNamesLength = NamelistHandler.LastNames.Count();
            int maleNamesLength = NamelistHandler.MaleNames.Count();
            Random rnd = Dice.Rnd;

            switch (nameScheme)
            {
                case "firstname + lastname":
                    name[0] = firstName[rnd.Next(firstNamesLength)];
                    name[1] = NamelistHandler.LastNames[rnd.Next(lastNamesLength)];
                    break;
                case "firstname":
                    name[0] = firstName[rnd.Next(firstNamesLength)];
                    break;
                case "firstname + malename":
                    name[0] = firstName[rnd.Next(firstNamesLength)];
                    name[1] = NamelistHandler.MaleNames[rnd.Next(maleNamesLength)];
                    break;
                case "firstname + malename + malename":
                    name[0] = firstName[rnd.Next(firstNamesLength)];
                    name[1] = NamelistHandler.MaleNames[rnd.Next(maleNamesLength)] + " " + NamelistHandler.MaleNames[rnd.Next(maleNamesLength)];
                    break;
                case "firstname + lastname + malename":
                    name[0] = firstName[rnd.Next(firstNamesLength)];
                    name[1] = NamelistHandler.LastNames[rnd.Next(lastNamesLength)] + " " + NamelistHandler.MaleNames[rnd.Next(maleNamesLength)];
                    break;
                case "lastname":
                    name[0] = "";
                    name[1] = NamelistHandler.LastNames[rnd.Next(lastNamesLength)];
                    break;
            }
            return name;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Cthulhu_NPC_Gen_v2
{
    class NamelistHandler
    {
        public static List<string> MaleNames { get; set; }
        public static List<string> FemaleNames { get; set; }
        public static List<string> LastNames { get; set; }

        public static void ParseNameList(Ethnicity ethnicity)
        {
            string path = @"..\..\res\data\" + ethnicity + ".txt";

            MaleNames = new List<string>();
            FemaleNames = new List<string>();
            LastNames = new List<string>();
            Console.WriteLine("---> NamelistHandler created. Path = \"" + path + "\"");

            if (File.Exists(path))
            {
                StreamReader sr = File.OpenText(path);
                Console.WriteLine("--> Filestream opened.");
                string s = "";

                // nameIdent: 1 = Male name; 2 = Female name; 3 = Last name
                int nameIdent = 0;

                while((s = sr.ReadLine()) != null)
                {
                    switch (s)
                    {
                        case "**FIRSTNAMES_MALE**":
                            nameIdent = 1;
                            Console.WriteLine("-> Male names added.");
                            continue;
                        case "**FIRSTNAMES_FEMALE**":
                            nameIdent = 2;
                            Console.WriteLine("-> Female names added.");
                            continue;
                        case "**LASTNAMES**":
                            nameIdent = 3;
                            Console.WriteLine("-> Last names added.");
                            continue;
                    }

                    switch (nameIdent)
                    {
                        case 1:
                            MaleNames.Add(s);
                            continue;
                        case 2:
                            FemaleNames.Add(s);
                            continue;
                        case 3:
                            LastNames.Add(s);
                            continue;
                        case 0:
                            Console.WriteLine("Wrong textfile formatting.");
                            break;
                    }
                }
                sr.Close();
                Console.WriteLine("--> Filestream closed.\n");
                //Console.WriteLine(ArraysToString());
            }
            else
            {
                Console.WriteLine("---> ERROR: File doesnt exist.");
            }
        }
        public string ArraysToString()
        {
            string names = "";

            names += "Male names\n----------\n";
            foreach (string name in MaleNames)
            {
                names += name + ", ";
            }
            names += "\n\n";

            names += "Female names\n----------\n";
            foreach (string name in FemaleNames)
            {
                names += name + ", ";
            }
            names += "\n\n";

            names += "Last names\n----------\n";
            foreach (string name in LastNames)
            {
                names += name + ", ";
            }
            names += "\n\n";

            return names;
        }
    }
}
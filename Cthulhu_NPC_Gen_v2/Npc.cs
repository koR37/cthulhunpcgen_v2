﻿using System.Collections.Generic;

namespace Cthulhu_NPC_Gen_v2
{
    public class Npc
    {
        // Basic information
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }
        public Archetype Archetype { get; set; }
        public Ethnicity Ethnicity { get; set; }

        // Basic attributes
        public int Strength { get; set; }
        public int Constitution { get; set; }
        public int Size { get; set; }
        public int Dexterity { get; set; }
        public int Appearance { get; set; }
        public int Intelligence { get; set; }
        public int Mana { get; set; }
        public int Education { get; set; }

        // Secondary Attributes
        public int Movement { get; set; }
        public int Luck { get; set; }
        public int Hitpoints { get; set; }
        public int Magicpoints { get; set; }
        public int Stature { get; set; }
        public int DamageBonus { get; set; }
        public int Stability { get; set; }

        // Job related variables
        public string Job { get; set; }
        public string Contacts { get; set; }
        public int FinancePoints { get; set; }
        public int SkillPoints { get; set; }
        public int HobbyPoints { get; set; }
        public List<Skill> SkillSet { get; set; }

        public Npc() { }

        public Npc(int age, Gender gender, Archetype archetype, Ethnicity ethnicity)
        {
            Age = age;
            Gender = gender;
            Archetype = archetype;
            Ethnicity = ethnicity;
        }

        public override string ToString()
        {
            return  "\n=======================================\n\n" +
                    "Name: " + Firstname + " " + Lastname + ", " + Age + ", " + Gender + ", " + Archetype + ", " + Ethnicity + "\n" +
                    "ST: " + Strength + " | " + "KO: " + Constitution + " | " + "GR: " + Size + " | " + "GE: " + Dexterity + " | " + "ER: " + Appearance + " | " + "IN: " + Intelligence + " | " + "MA: " + Mana + " | " + "BI: " + Education + "\n" +
                    "BW: " + Movement + " | " + "GL: " + Luck + " | " + "TP: " + Hitpoints + " | " + "MP: " + Magicpoints + " | " + "Statur: " + Stature + " | " + "+DMG: " + DamageBonus + " | " + "STA: " + Stability + "\n" +
                    "---------------------------------------\n" + 
                    "Job: " + Job + "\n" +
                    "Kontakte: " + Contacts + "\n" +
                    "Finanzen: " + FinancePoints + " | Fertigkeitenpunkte: " + (SkillPoints - FinancePoints) + " | Hobbypunkte: " + HobbyPoints + "\n" +
                    "Fertigkeiten: \n" + Skill.ToStringList(SkillSet) +
                    "\n=======================================";
        }
    }
}
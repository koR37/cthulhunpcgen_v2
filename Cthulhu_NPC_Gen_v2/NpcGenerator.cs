﻿namespace Cthulhu_NPC_Gen_v2
{
    public class NpcGenerator
    {
        public static Npc GenerateNpc(int age, Gender gender, Archetype archetype, Ethnicity ethnicity)
        {
            Npc npc = new Npc(age, gender, archetype, ethnicity);
            SkillSetHandler skillSetHandler = new SkillSetHandler();

            // Calculate attributes and secondary attributes.
            AttributeGenerator.CubeWeightedAttributes(npc);

            // Check if ethnicity changed.
            if (MainWindow.LastNpc == null || MainWindow.LastNpc.Ethnicity != ethnicity)
            {
                NamelistHandler.ParseNameList(ethnicity);
            }

            // Get Npc first- and lastname.
            string[] name = NameGenerator.GenerateName(npc);
            npc.Firstname = name[0];
            npc.Lastname = name[1];

            // Get Job and calculate skill values.
            skillSetHandler.BuildSkillSet(npc);
            SkillCalculator skillCalculator = new SkillCalculator(npc);
            skillCalculator.SetJobSkillValues();

            return npc;
        }
    }
}
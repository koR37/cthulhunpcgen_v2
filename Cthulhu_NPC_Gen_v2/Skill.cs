﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections;

namespace Cthulhu_NPC_Gen_v2
{
    public class SortSkill : IComparer<Skill>
    {
        public int Compare(Skill x, Skill y)
        {
            if (x.Value > y.Value)
            {
                return -1;
            }
            else if (x.Value < y.Value)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }

    public class Skill
    {
        public struct Specialisation
        {
            [JsonProperty("Skill")]
            public string Name { get; set; }
            [JsonProperty("BaseValue")]
            public int BaseValue { get; set; }
            public string Type { get; set; }

            public override string ToString()
            {
                return Name + " (" + BaseValue + "%)";
            }
        }

        [JsonProperty("Skill")]
        public string Name { get; set; }
        [JsonProperty("BaseValue")]
        public int BaseValue { get; set; }
        [JsonProperty("Type")]
        public string Type { get; set; }
        [JsonProperty("Specialisations")]
        public Specialisation[] Specialisations { get; set; }

        // True if BaseValue got already added during calculation.
        public bool UsedBaseValue { get; set; }
        // Actual skill value.
        public int Value { get; set; }

        public override string ToString()
        {
            string specialisations = "";
            if (Specialisations != null)
            {
                specialisations = "\t->\tSpecialisations: " + string.Join(", ", Specialisations);
            }

            return Name + "\n**(" + Value + "%, " + Value/2 + ", " + Value/4 +  ")**" + specialisations;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (GetType() != obj.GetType())
            {
                return false;
            }

            Skill skill = (Skill)obj;
            return skill.Name == Name && skill.BaseValue == BaseValue;
        }

        // Wtf
        public override int GetHashCode()
        {
            var hashCode = 367809352;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + BaseValue.GetHashCode();
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Type);
            hashCode = hashCode * -1521134295 + EqualityComparer<Specialisation[]>.Default.GetHashCode(Specialisations);
            return hashCode;
        }

        public static string ToStringList(List<Skill> skillSet)
        {
            string output = "";
            foreach (Skill skill in skillSet)
            {
                output += "\t(" + skill.BaseValue + "%)\t" + skill.Name + "\n";
            }
            return output;
        }
    }
}
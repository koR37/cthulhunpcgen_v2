﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cthulhu_NPC_Gen_v2
{
    public class SkillCalculator
    {
        public Npc Npc;
        public Random Rnd;
        public SkillSetHandler SkillSetHandler;
        public Skill[] SkillList;
        public int SkillPointBank;
        public int HobbyPointBank;

        public int MinHobbySkills { get; set; }
        public int MaxHobbySkills { get; set; }

        // For debugging
        private int totalExpensesJob = 0;
        private int totalExpensesHobby = 0;

        public SkillCalculator(Npc npc)
        {
            Npc = npc;
            SkillSetHandler = new SkillSetHandler();
            Rnd = Dice.Rnd;
            SkillList = JsonHandler.SkillList;

            SkillPointBank = npc.SkillPoints - npc.FinancePoints;
            HobbyPointBank = npc.HobbyPoints;

            MinHobbySkills = HobbyPointBank / 25;
            MaxHobbySkills = MinHobbySkills + 1;
        }

        public void SetJobSkillValues()
        {
            // Spend SkillPoints for job skills.
            while (SkillPointBank > 0)
            {
                int randomIndex = Rnd.Next(Npc.SkillSet.Count);
                Skill skill = Npc.SkillSet[randomIndex];

                int expense = AddPointsToSkill(skill, false);
                skill.Value += expense;
            }
            
            // Add general skills to Npc.SkillSet.
            AddGeneralSkills();

            // Calculate amount of hobby skills and add to Npc.SkillSet.
            int hobbySkillCount = Rnd.Next(MinHobbySkills, MaxHobbySkills);
            GetHobbySkills(hobbySkillCount);

            // Spend HobbyPoints for hobby skills.
            while (HobbyPointBank > 0)
            {
                int randomIndex = Rnd.Next(Npc.SkillSet.Count);
                Skill skill = Npc.SkillSet[randomIndex];

                int expense = AddPointsToSkill(skill, true);
                skill.Value += expense;
                hobbySkillCount--;
            }

            Console.WriteLine("\n######### Total expenses ---> Jobskills: {0}\tHobbyskills: {1} #########\n", totalExpensesJob, totalExpensesHobby);

            Console.WriteLine("+++ Set following skill values:");
            foreach (Skill skill in Npc.SkillSet)
            {
                //if (skill.Value == 0)
                //{
                //    skill.Value = skill.BaseValue;
                //}
                Console.WriteLine("\t # ({0}%) -> {1}\t{2}", skill.BaseValue, skill.Value, skill.Name);
            }

            // Debug.
            int combinedValues = 0;
            int combinedBaseValues = 0;
            foreach (Skill skill in Npc.SkillSet)
            {
                combinedValues += skill.Value;
                combinedBaseValues += skill.BaseValue;
            }
            
            int totalExpenses = totalExpensesHobby + totalExpensesJob;
            int result = totalExpenses - combinedBaseValues;
            //Console.WriteLine("Calculation correct?: {0} - {1} = {2}", combinedValues, combinedBaseValues, result);
            Console.WriteLine("Calculation correct?: {0} - {1} = {2}", totalExpenses, combinedBaseValues, result);

            Console.WriteLine("+++ HobbyPoints remaining: {0}\t\t SkillPoints remaining: {1}\n", HobbyPointBank, SkillPointBank);
        }

        public int AddPointsToSkill(Skill skill, bool isHobby)
        {
            int expense = 0;
            int pointBank;

            CheckGeneralSkills();

            if (skill.Name == "Finanzkraft")
            {
                return 0;
            }

            // Check if skill value has already been set.
            if (!skill.UsedBaseValue)
            {
                // Start the skill value with basevalue.
                skill.Value = skill.BaseValue;
                skill.UsedBaseValue = true;
            }

            // Set pointbank accordingly.
            if (isHobby)
            {
                pointBank = HobbyPointBank;
            }
            else
            {
                pointBank = SkillPointBank;
            }

            // Immediately spend rest points if below 5 because value finding loop generates numbers in steps of 5.
            if (pointBank < 5 && skill.Value + pointBank <= 90)
            {
                expense = pointBank;
                pointBank = 0;
                // Set banks to zero.
                if (isHobby)
                {
                    HobbyPointBank = pointBank;
                    totalExpensesHobby += expense;
                }
                else
                {
                    SkillPointBank = pointBank;
                    totalExpensesJob += expense;
                }
                Console.WriteLine("Rest {0} spent on\t:\t{1}\t---> Current value: {2} (UsedBaseValue ({3}): {4})", expense, skill.Name, skill.Value + expense, skill.BaseValue, skill.UsedBaseValue);
                return expense;
            }

            // Weight value range away from 90.
            int maxValue = 75;
            int multiplier = 10;
            if (Rnd.Next(101) == 1)
            {
                maxValue = 90;
                multiplier = 19;
            }

            // Search for a legit expense.
            do
            {
                if (skill.Value >= maxValue - 5)
                {
                    return 0;
                }
                expense = Rnd.Next(multiplier) * 5;

            } while (skill.Value + expense > maxValue || expense > pointBank);

            // Substract expense from according point bank.
            if (isHobby)
            {
                HobbyPointBank -= expense;
                totalExpensesHobby += expense;
            }
            else
            {
                SkillPointBank -= expense;
                totalExpensesJob += expense;
            }

            if (expense != 0)
            {
                Console.WriteLine("Spent {0} on\t:\t{1}\t---> Current value: {2} (UsedBaseValue ({3}): {4})", expense, skill.Name, skill.Value + expense, skill.BaseValue, skill.UsedBaseValue);
            }
            return expense;
        }

        public void CheckGeneralSkills()
        {
            Skill dodging = SkillSetHandler.GetSkillFromString("Ausweichen");
            Skill motherTongue = SkillSetHandler.GetSkillFromString("Muttersprache");

            if (Npc.SkillSet.Contains(dodging))
            {
                dodging.BaseValue = Npc.Dexterity / 2;
                dodging.Value = dodging.BaseValue;
                dodging.UsedBaseValue = true;
            }

            if (Npc.SkillSet.Contains(motherTongue))
            {
                motherTongue.BaseValue = Npc.Education;
                motherTongue.Value = motherTongue.BaseValue;
                motherTongue.UsedBaseValue = true;
            }
        }

        public void AddGeneralSkills()
        {
            Skill dodging = SkillSetHandler.GetSkillFromString("Ausweichen");
            dodging.BaseValue = Npc.Dexterity / 2;
            dodging.Value = dodging.BaseValue;
            dodging.UsedBaseValue = true;

            Skill motherTongue = SkillSetHandler.GetSkillFromString("Muttersprache");
            motherTongue.BaseValue = Npc.Education;
            motherTongue.Value = motherTongue.BaseValue;
            motherTongue.UsedBaseValue = true;

            Skill finances = SkillSetHandler.GetSkillFromString("Finanzkraft");
            finances.BaseValue = 0;
            finances.Value = Npc.FinancePoints;
            finances.UsedBaseValue = true;

            if (!Npc.SkillSet.Contains(dodging))
            {
                Npc.SkillSet.Add(dodging);
                Console.WriteLine("\n\t\t+ Added general skill: {0} ({1}%)", dodging.Name, dodging.BaseValue);
            }
            if (!Npc.SkillSet.Contains(motherTongue))
            {
                Npc.SkillSet.Add(motherTongue);
                Console.WriteLine("\t\t+ Added general skill: {0} ({1}%)", motherTongue.Name, motherTongue.BaseValue);
            }
            if (!Npc.SkillSet.Contains(finances))
            {
                Npc.SkillSet.Add(finances);
                Console.WriteLine("\t\t+ Added general skill: {0} ({1}%)", finances.Name, finances.BaseValue);
            }
        }

        public void GetHobbySkills(int hobbySkillAmount)
        {
            Console.WriteLine("\n");

            int hobbySkillCount = hobbySkillAmount;
            Skill randomSkill = new Skill();

            while (hobbySkillCount != 0)
            {
                do
                {
                    randomSkill = GetRandomSkill();
                } while (Npc.SkillSet.Contains(randomSkill));

                if (IsLegitSkill(randomSkill))
                {
                    Console.WriteLine("\t\t+ Added hobby skill: {0} ({1}%)", randomSkill.Name, randomSkill.BaseValue);
                    Npc.SkillSet.Add(randomSkill);
                    hobbySkillCount--;
                }
            }
            Console.WriteLine("\n");
        }

        public bool IsLegitSkill(Skill skill)
        {
            return skill.Type != "special";
        }

        public Skill GetRandomSkill()
        {
            int randomIndex = Rnd.Next(SkillList.Count());

            if (SkillList[randomIndex].Specialisations != null)
            {
                return GetRandomSpecialisation(SkillList[randomIndex]);
            }
            else
            {
                return SkillList[randomIndex];
            }
        }

        public Skill GetRandomSpecialisation(Skill skill)
        {
            return ConvertSpecialisationToSkill(skill.Specialisations[Rnd.Next(skill.Specialisations.Length)], skill.Type, skill.Name);
        }

        // Converts Specialisation to Skill
        public Skill ConvertSpecialisationToSkill(Skill.Specialisation specialisation, string type, string skillName)
        {
            Skill skill = new Skill();
            skill.Name = "(" + skillName + ") " + specialisation.Name;
            skill.BaseValue = specialisation.BaseValue;
            skill.Type = type;
            skill.Specialisations = null;
            return skill;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cthulhu_NPC_Gen_v2
{
    public class SkillSetHandler
    {
        public Random Rnd;
        public Skill[] SkillList;
        public Job[] JobList;
        public List<Skill> SoftSkills;
        public List<Skill> NpcSkillSet;

        public SkillSetHandler()
        {
            JobList = JsonHandler.JobList;
            SkillList = JsonHandler.SkillList;
            Rnd = Dice.Rnd;
            SoftSkills = new List<Skill>();
            NpcSkillSet = new List<Skill>();
        }

        public void BuildSkillSet(Npc npc)
        {
            // Reset values of skills
            foreach (Skill skill in SkillList)
            {
                skill.Value = 0;
                skill.UsedBaseValue = false;
            }

            // Get random job.
            Job currentJob = JobList[Rnd.Next(JobList.Length)];
            npc.Job = currentJob.Name;

            // Debug part
            //#######################################
            //foreach (Job job in JobList)
            //{
            //    if (job.Name == "Kopfgeldjäger")
            //    {
            //        currentJob = job;
            //        npc.Job = currentJob.Name;
            //    }
            //}
            // #######################################
            // Debug part end

            // Initialise SoftSkills.
            foreach (Skill currentSkill in SkillList)
            {
                if (currentSkill.Name == "Charme" || currentSkill.Name == "Einschüchtern" || currentSkill.Name == "Überreden" || currentSkill.Name == "Überzeugen")
                {
                    SoftSkills.Add(currentSkill);
                }
            }

            // Add random SoftSkills to NpcSkillSet.
            while(NpcSkillSet.Count != currentJob.SoftSkillAmount)
            {
                int rndIndex = Rnd.Next(SoftSkills.Count - 1);
                if (!NpcSkillSet.Contains(SoftSkills[rndIndex]))
                {
                    NpcSkillSet.Add(SoftSkills[rndIndex]);
                }
            }

            // Add Skills to NpcSkillSet.
            foreach (string currentSkillString in currentJob.Skills)
            {
                NpcSkillSet.Add(GetSkillFromString(currentSkillString));
            }

            // Add SkillChoiceAmount SkillChoices to NpcSkillSet.
            if (currentJob.SkillChoiceAmount != 0)
            {
                Skill[] skillChoices = GetRandomSkillChoices(currentJob);
                foreach (Skill skill in skillChoices)
                {
                    NpcSkillSet.Add(skill);
                }
            }

            // Get highest job defined attribute if needed.
            int highestAttributeValue = 0;
            if (currentJob.SecondAttributes[0] != "none")
            {
                foreach (string attributeName in currentJob.SecondAttributes)
                {
                    if (highestAttributeValue < GetAttributeByString(npc, attributeName))
                    {
                        highestAttributeValue = GetAttributeByString(npc, attributeName);
                    }
                }
            }

            // Random number within financial limits defined by job.
            npc.FinancePoints = Rnd.Next(currentJob.Finances[0], currentJob.Finances[1]);
            // Calculate SkillPoints and HobbyPoints of job for current Npc.
            npc.SkillPoints = npc.Education * currentJob.EducationModifier + highestAttributeValue * currentJob.SecondAttributeModifer;
            npc.HobbyPoints = npc.Intelligence * 2;
            npc.SkillSet = NpcSkillSet;
            npc.Contacts = currentJob.Contacts;

            Console.WriteLine("\n\n############ Generated Npc ############" + npc + "\n#######################################\n");
        }

        // Matches string to attribute value of given Npc.
        public int GetAttributeByString(Npc npc, string attributeName)
        {
            int attributeValue = 0;
            switch (attributeName)
            {
                case "ST":
                    attributeValue = npc.Strength;
                    break;
                case "KO":
                    attributeValue = npc.Constitution;
                    break;
                case "GR":
                    attributeValue = npc.Size;
                    break;
                case "GE":
                    attributeValue = npc.Dexterity;
                    break;
                case "ER":
                    attributeValue = npc.Appearance;
                    break;
                case "IN":
                    attributeValue = npc.Intelligence;
                    break;
                case "MA":
                    attributeValue = npc.Mana;
                    break;
                case "BI":
                    attributeValue = npc.Education;
                    break;
            }
            return attributeValue;
        }

        public Job GetJobByName(string name)
        {
            Job job = new Job();
            foreach (Job currentJob in JobList)
            {
                if (currentJob.Name == name)
                {
                    return currentJob;
                }
            }
            return job;
        }

        // Gets a Skill from given string.
        // Tested. Works so far.
        public Skill GetSkillFromString(string input)
        {
            Skill skill = new Skill();
            string currentString = input;

            // Special cases handling.
            if (currentString.Contains("Studiengebiet_"))
            {
                return GetRandomSkillOfType("academic");
            }
            if (currentString.Contains("Naturwissenschaft_"))
            {
                return GetRandomSpecialisation("Naturwissenschaft");
            }

            // Handle choices. Cleanup string, handle choices.
            if (currentString.Contains("#"))
            {
                currentString = GetChoice(currentString);
            }
            if (currentString.Contains("("))
            {
                currentString = GetChoice(currentString);
            }

            // Loops through SkillList and look for match.
            foreach (Skill currentSkill in SkillList)
            {
                // Look for normal skill without choices.
                if (currentSkill.Name == currentString)
                {
                    if (currentSkill.Specialisations != null)
                    {
                        return GetRandomSpecialisation(currentString);
                    }
                    else
                    {
                        return currentSkill;
                    }
                }

                // Look through specialisations.
                if (currentSkill.Specialisations != null)
                {
                    foreach (Skill.Specialisation currentSpecialisastion in currentSkill.Specialisations)
                    {
                        if (currentSpecialisastion.Name == currentString)
                        {
                            // Specialisation found, convert to Skill.
                            return SpecialisationToSkill(currentSkill, currentSpecialisastion);
                        }
                    }
                }
            }
            return skill;
        }

        // Returns a random choice. Uses other seperator functions.
        // Tested. Works so far.
        public string GetChoice(string input)
        {
            string currentString = input;

            if (currentString.Contains('#'))
            {
                string[] choices = currentString.Split('#');
                string choice = choices[Rnd.Next(choices.Length)];

                if (choice.Contains("("))
                {
                    currentString = GetParenthesisContent(choice);
                }
                return choice;
            }
            else if (input.Contains("("))
            {
                return GetParenthesisContent(currentString);
            }
            else
            {
                return currentString;
            }
        }

        // Returns a random string from multiple strings within parenthesis which are separated by '|'.
        // Tested. Works so far.
        public string GetParenthesisContent(string input)
        {
            if (input.Contains('('))
            {
                // Remove parenthesis.
                string[] tmpArray = input.Split('(');
                string cleanInput = tmpArray[1].Replace(")", "");

                // Check for inner choices.
                if (cleanInput.Contains('|'))
                {
                    return GetInnerChoice(cleanInput);
                }
                return cleanInput;
            }
            else
            {
                return input;
            }
        }

        // Support function for GetParenthesisContent to return a random inner choice.
        // Tested. Works so far.
        public string GetInnerChoice(string input)
        {
            if (input.Contains("|"))
            {
                string[] choices = input.Split('|');
                return choices[Rnd.Next(choices.Length)];
            }
            else
            {
                return input;
            }
        }

        // Gets an Array of SkillChoiceAmount random Skills.
        // Tested. Works so far.
        public Skill[] GetRandomSkills(int skillChoiceAmount)
        {
            Skill[] skills = new Skill[skillChoiceAmount];

            for (int i = 0; i < skillChoiceAmount; i++)
            {
                // Set random Skill, retry if it already exists.
                int randomIndex = Rnd.Next(SkillList.Length);
                while (skills.Contains(SkillList[randomIndex]) || NpcSkillSet.Contains(SkillList[randomIndex]))
                {
                    randomIndex = Rnd.Next(SkillList.Length);
                }

                Skill currentSkill = SkillList[randomIndex];

                // If Skill has Specialisations, set a random one.
                if (currentSkill.Specialisations != null)
                {
                    int tmpIndex = Rnd.Next(currentSkill.Specialisations.Length);
                    currentSkill = SpecialisationToSkill(currentSkill, currentSkill.Specialisations[tmpIndex]);
                }

                skills[i] = currentSkill;
            }
            return skills;
        }

        // Get SkillChoiceAmount random Skills of SkillChoices given by a Job.
        // Tested. Works so far. Careful returns null if SkillChoiceAmount = 0, do not call if so.
        public Skill[] GetRandomSkillChoices(Job job)
        {
            Skill[] skills = new Skill[job.SkillChoiceAmount];

            // Academic Specialisations
            if (job.SkillChoices[0].Contains("akademische") || job.SkillChoices[0].Contains("Studiengebiete"))
            {
                return GetRandomSkillOfTypeArray("academic", skills.Length);
            }

            // Personal Specialisations
            if (job.SkillChoices[0].Contains("persönliche"))
            {
                return GetRandomSkills(job.SkillChoiceAmount);
            }

            // Personal Science Specialisations
            if (job.SkillChoices[0].Contains("Naturwissenschaften"))
            {
                Skill[] scienceSkills = GetSpecialisationsOfSkill("Naturwissenschaft");

                for (int i = 0; i < skills.Length; i++)
                {
                    while (skills[i] == null)
                    {
                        int randomIndex = Rnd.Next(scienceSkills.Length);
                        if (!skills.Contains(scienceSkills[randomIndex]))
                        {
                            skills[i] = scienceSkills[randomIndex];
                        }
                    }
                }
                return skills;
            }

            // Multiple choices
            for (int i = 0; i < skills.Length; i ++)
            {
                Skill skillChoice = new Skill();
                do
                {
                    skillChoice = GetSkillFromString(job.SkillChoices[Rnd.Next(skills.Length)]);
                } while (skills.Contains(skillChoice));
                skills[i] = skillChoice;
            }

            return skills;
        }

        // Returns a random Skill of a type, including specialisations.
        // Tested. Works so far.
        public Skill GetRandomSkillOfType(string type)
        {
            Skill skill = null;
            string currentType = type;
            int specialCase = Rnd.Next(101);
            int randomIndex = Rnd.Next(SkillList.Length);

            while (SkillList[randomIndex].Type != currentType || skill == null)
            {
                randomIndex = Rnd.Next(SkillList.Length);
                if (SkillList[randomIndex].Specialisations != null)
                {
                    skill = GetRandomSpecialisation(SkillList[randomIndex].Name);
                }
                else
                {
                    skill = SkillList[randomIndex];
                }

                // Academic preferences weighted towards scientific specialisations.
                int academicWeight = Rnd.Next(4);
                if(academicWeight < 3 && currentType == "academic")
                {
                    skill = GetRandomSpecialisation("Naturwissenschaft");
                }
                
                // Rare and uncommon skills. 1 percent chance.
                if (specialCase == 1)
                {
                    currentType = "special";
                }
                else
                {
                    currentType = type;
                }
            }
            return skill;
        }

        // Returns an array of amount random skills of type.
        // Tested. Works so far.
        public Skill[] GetRandomSkillOfTypeArray(string type, int amount)
        {
            Skill[] skills = new Skill[amount];
            Skill randomSkill = new Skill();

            for (int i = 0; i < amount; i++)
            {
                while (skills[i] == null)
                {
                    randomSkill = GetRandomSkillOfType(type);

                    if (!(skills.Contains(randomSkill)))
                    {
                        skills[i] = randomSkill;
                    }
                }
            }

            return skills;
        }

        // Returns a random specialisation of a Skill by string.
        // Tested. Works so far.
        public Skill GetRandomSpecialisation(string skillName)
        {
            Skill[] specialisations = GetSpecialisationsOfSkill(skillName);
            return specialisations[Rnd.Next(specialisations.Length)];
        }

        // Looks for the Specialisations of a certain Skill by given name.
        // Tested. Works so far.
        public Skill[] GetSpecialisationsOfSkill(string skillName)
        {
            Skill[] skills = null;

            foreach (Skill currentSkill in SkillList)
            {
                if (currentSkill.Name == skillName && currentSkill.Specialisations != null)
                {
                    skills = new Skill[currentSkill.Specialisations.Length];

                    for (int i = 0; i < currentSkill.Specialisations.Length; i++)
                    {
                        skills[i] = new Skill();
                        skills[i] = SpecialisationToSkill(currentSkill, currentSkill.Specialisations[i]);
                    }
                    return skills;
                }
            }
            return skills;
        }

        public Skill SpecialisationToSkill(Skill currentSkill, Skill.Specialisation specialisation)
        {
            Skill skill = new Skill();

            skill.Name = "(" + currentSkill.Name + ") " + specialisation.Name;
            skill.Type = currentSkill.Type;
            skill.Specialisations = null;
            skill.UsedBaseValue = false;

            if (currentSkill.BaseValue != specialisation.BaseValue)
            {
                skill.BaseValue = specialisation.BaseValue;
            }
            else
            {
                skill.BaseValue = currentSkill.BaseValue;
            }

            return skill;
        }
    }
}
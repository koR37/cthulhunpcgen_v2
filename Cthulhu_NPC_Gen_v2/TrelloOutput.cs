﻿namespace Cthulhu_NPC_Gen_v2
{
    class TrelloOutput
    {
        public static string PrintTrelloOutput(Npc npc)
        {
            string output =
                "###" + npc.Firstname + " " + npc.Lastname + "\n" +
                "**Beschreibung:**" + "\n...\n" +
                "\n---\n" +
                "_Alter_: **" + npc.Age + "**\n" +
                "_Beruf_: **" + npc.Job + "**\n" +
                "_Herkunft_: **" + npc.Ethnicity + "**\n\n" +
                "---\n" +
                "_Stärke_: **" + npc.Strength + "**\n" +
                "_Konstitution_: **" + npc.Constitution + "**\n" +
                "_Größe_: **" + npc.Size + "**\n" +
                "_Geschick_: **" + npc.Dexterity + "**\n" +
                "_Erscheinung_: **" + npc.Appearance + "**\n" +
                "_Intelligenz_: **" + npc.Intelligence + "**\n" +
                "_Mana_: **" + npc.Mana + "**\n" +
                "_Bildung_: **" + npc.Education + "**\n\n" +
                "---\n" +
                "_Bewegungsweite_: **" + npc.Movement + "**\n" +
                "_Glück_: **" + npc.Luck + "**\n" +
                "_Trefferpunkte_: **" + npc.Hitpoints + "**\n" +
                "_Magiepunkte_: **" + npc.Magicpoints + "**\n" +
                "_Statur_: **" + npc.Stature + "**\n" +
                "_Schadensbonus_: **" + npc.DamageBonus + "**\n" +
                "_Stabilität_: **" + npc.Stability + "**\n\n" +
                "---\n" +
                "**Berufsfertigkeiten:**" + "\n>" +
                string.Join("\n", npc.SkillSet);

            return output;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cthulhu_NPC_Gen_v2.res
{
    class SkillStringHandler
    {
        public static Random rnd = new Random(Guid.NewGuid().GetHashCode());

        public static string GetRndFirstChoice(string input)
        {
            if (input.Contains('|'))
            {
                string[] choices = input.Split(new[] { '|' }, 2);

                return choices[rnd.Next(choices.Length-1)];
            }
            else
            {
                return "No choices.";
            }
        }

        public static string GetRndParenthesisContent(string input)
        {
            if (input.Contains('('))
            {
                string[] tmpArray = input.Split('(');
                string cleanInput = tmpArray[1].Replace(")", "");

                if (cleanInput.Contains('|'))
                {
                    string[] skillChoices = cleanInput.Split('|');

                    return skillChoices[rnd.Next(skillChoices.Length-1)];
                }
                else
                {
                    if (cleanInput == "beliebig")
                    {
                        return "any";
                    }

                    return cleanInput;
                }
            }
            else
            {
                Console.WriteLine("+++ String does not contain parenthesis. +++");
                return "No legit skill.";
            }
        }
    }
}